/**
 * @author Chris Hinson
 * @version 1.0
 * Monthly pricing tool for internet service
**/
public class main
{
    public static void main(String[] args)
    {
        //instantiate the userInput class to gain acces to the input methods
        userInput in = new userInput();

        //define String for what plan the user has
        String plan;

        //define integer for how many hours the user has used this month
        int hours;

        //define the doubles that make up the users final cost
        //totalCharge = basecharge + overcharge
        double overcharge,basecharge,totalCharge;

        //call the function to get the User's plan
        plan = in.getPlan();

        //call the function to get the user's number of hours used
        hours = in.getHours();

        //call the function to get the user's base monthly charge,
        //based on the plan and number of hours used they have already in put
        basecharge = in.baseCharge(plan);

        //call the function to get the user's overage cost,
        //again based on the plan and number of hours they have already input
        overcharge = in.overCharge(plan, hours);

        //add the base cahrge and over charge to get the user's final cost
        totalCharge = basecharge + overcharge;

        //print the user's final cost, and thank them for using the service
        System.out.println("You total charge for the month is $" + totalCharge + "\nThank you for choosing GreatISP.");
    }
}
