/**
 *@author Chris Hinson
 * This class contains all the methods nessecary for getting all user input
**/

//necessary for getting input from the keyboard
import java.util.Scanner;

public class userInput
{
    //instantiate the scanner class to get keyboard input
    Scanner keyboard = new Scanner(System.in);

    //this function, when called, prompts the user for their subscription plan,
    //and ensures that only a valid plan may be chosen
    //then returns that plan as a string
    public String getPlan()
    {
        //boolean for input validation loop
        Boolean validInput = null;
        //string for the user's plan. Returned when defined
        String plan;

        //Prompts the user for their input then defines the plan variable in accordance
        System.out.println("Please enter your subscription level \n A, B, or C");
        plan = keyboard.nextLine();

        //input validation loop, used do while because it must execute at least once
        do {
            //check if the entered value is valid
            if (validate.validString(plan, "(?i)[ABC]") == Boolean.TRUE) {
                //make sure the entered plan is uppercase, for comaprison purposes later on
                plan = plan.toUpperCase();
                //thank the user for their input
                System.out.println("Ok, thank you class " + plan + " customer");
                //break the validation loop
                validInput = Boolean.TRUE;
            } else {
                //tell the user they haven't entered a valid plan ,and remind them of the acceptable input values
                System.out.println("Sorry, that's not a valid subscription plan, please choose plan A, B, or C");
                //get the user's new input
                plan = keyboard.nextLine();
            }
        }
        //continue thisvalue checking until the validation loop variable is assigned the value true
        while (validInput != Boolean.TRUE);

        //return the user's entered plan, validated and sanitized for later use
        return plan;
    }

    //method for getting the amount of hours the user has used this month
    //returns the number of hours they've used
    public int getHours()
    {
        //input validation loop variable
        Boolean validated = Boolean.FALSE;

        //variable for number of hours used
        int hours;

        //input validation loop
        //do while loop because it must always execute at least once
        do {

            //prompt the user for the amount of hours they've used
            System.out.println("Please enter the amount of hours you have used this month.");

            //if the entered value is not an int, alert the user and ask for a new value
            while (!keyboard.hasNextInt())
            {
                //if the entered vale is a double, tell the user they can only enter whole hours
                while (keyboard.hasNextDouble())
                {
                    System.out.println("Sorry, only whole numbers of hours are acceptable");
                    //get the user's new value
                    keyboard.next();
                }
                System.out.println("Sorry, that's not a valid number, please try again");
                //get the user's new value
                keyboard.next();
            }

            //get the user's next value, will throw an error if not an int
            hours = keyboard.nextInt();

            //set validation variable to true to break validation loop
            validated = Boolean.TRUE;

        //repeat the input validatin until the validation variable is set to true
        } while (!validated);

        //return the amount of hours, validated
        return hours;
    }

    //function to determine the base charge based on the user's plan
    public double baseCharge(String bracket)
    {
        //define a double for the charge amount
        double baseCharge = 0.00;

        //if else statement to get charge based on plan

        //if plan A
        if (bracket.matches("A"))
        {
            //charge $9.95
            baseCharge = 9.95;
        }
        //if plan b
        else if (bracket.matches("B"))
        {
            //charge $13.95
            baseCharge = 13.95;
        }

        //if plan c
        else if (bracket.matches("C"))
        {
            //charge $19.95
            baseCharge = 19.95;
        }

        //return the base Charge amount
        return baseCharge;
    }

    //this function takes the user's bracket and hours, and returns their additional charges over their base charge
    public double overCharge(String bracket, int hours)
    {
        //this is the boolean that holds the actual charge to be returned.  Initialized to a dummy value of 0
        double overchargeAmount = 0.00;

        //if the user has plan A
        if (bracket.matches("A"))
        {
            //if the user has gone over their plans allotted monthly hours, charge them, otherwise, do not charge them
            //and congratulate them for staying under the max hours
            if (hours > 10)
            {
                System.out.println("You have gone over your plan's allotted hours per month, overage rates are $2 per hour");
                overchargeAmount = (hours-10) * 2.00;
                System.out.println("You have been charged an additional $" + overchargeAmount);
            }
            else
            {
                System.out.println("You have stayed under your allotted hours for the month, you will not face any additional charges");
                overchargeAmount = 0.00;
            }
        }
        //if the user has plan b
        else if (bracket.matches("B"))
        {
            //if the user has gone over their plans allotted monthly hours, charge them, otherwise, do not charge them
            //and congratulate them for staying under the max hours
            if (hours > 20) {
                System.out.println("You have gone over your plan's allotted hours per month, overage rates are $1 per hour");
                overchargeAmount = (hours-20) * 2.00;
                System.out.println("You have been charged an additional $" + overchargeAmount);
            }
            else
            {
                System.out.println("You have stayed under your allotted hours for the month, you will not face any additional charges");
                overchargeAmount = 0.00;
            }
        }
        //if the user has plan c
        else if (bracket.matches("C"))
        {
            //the user cannot be overcharged if they have plan c
            overchargeAmount = 0.00;
        }

        //return the overchargeAmount
        return overchargeAmount;
    }
}

