/**
 * @author Chris Hinson
 * A simple class to compare a string to a regex and determine whether it is valid
 * returns a boolean
**/

//necessary library for advanced regex comparison
import java.util.regex.*;

public class validate
{
    public static Boolean validString(String in, String regex)
    {
        //defines the regex that the input should be matched to
        Pattern p = Pattern.compile(regex);

        //defines the string that should be being compared to the regex
        Matcher m = p.matcher(in);

        //if statement based on the matcher class matches
        //if the string matches the regex, return true, otherwise, return false
        if (m.matches())
        {
            return Boolean.TRUE;
        }
        else
        {
            return Boolean.FALSE;
        }
    }

}
